package course.project;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FinalCostView extends JPanel {
    private JButton summaryButton;
    private JTextField totalField;

    public FinalCostView(){
        setLayout(new FlowLayout());
        add(new JLabel("Итого к оплате"));
        totalField = new JTextField(15);
        summaryButton = new JButton ("Оформить заказ");
        add(totalField);
        add(summaryButton);
    }
    void totalSummaryActionListener(ActionListener buttonAction){
        summaryButton.addActionListener(buttonAction);
    }
    void setTotalFeild(String totalCost){
        totalField.setText(totalCost);
    }
}

