package course.project;
import javax.swing.*;

public class OrderView extends JFrame {
    private JTextArea summary_text;
    public OrderView(){
        super ("Сведения о заказе");
        setSize (400,300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        summary_text = new JTextArea();
        getContentPane().add(summary_text);
        setVisible(false);
    }
    public void set_textarea (String text){
        summary_text.setText(text);
    }
    public void set_visibility (boolean visible){
        this.setVisible(visible);
    }

}
