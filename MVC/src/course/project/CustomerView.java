package course.project;

import javax.swing.*;

public class CustomerView extends JPanel{

    private JTextField nameField;
    private JTextField phoneField;

    public CustomerView() {

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(new JLabel("Оформление заказа"));

        add(new JLabel("Имя: "));
        nameField = new JTextField(15);
        add(nameField);

        add(new JLabel("Номер телефона: "));
        phoneField = new JTextField(15);
        add(phoneField);

        add(new JLabel(" "));
        add(new JLabel(" "));
    }
    public String _getName(){
        return nameField.getText();
    }
    public String getNumber(){
        return phoneField.getText();
    }
}
