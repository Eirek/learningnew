package course.project;

import javax.swing.*;
import javax.swing.event.ChangeListener;

public class View extends JFrame {

    private JTabbedPane tabbedPane;

    protected CustomerView customerView = new CustomerView();
    protected TypeView typeView = new TypeView();
    protected IngredientsView ingredientsView = new IngredientsView();
    protected FinalCostView finalCostView = new FinalCostView();

    public View(){
        super("Форма заказа");

        setSize(500, 200);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        tabbedPane = new JTabbedPane();

        tabbedPane.add("Имя", customerView);
        tabbedPane.add("Выбор пиццы", typeView);
        tabbedPane.add("Ингредиенты", ingredientsView);
        tabbedPane.add("Итого к оплате", finalCostView);

        add(tabbedPane);

        setVisible(true);
    }

    public void updateTotalCost(ChangeListener listener){
        tabbedPane.addChangeListener(listener);
    }
}
