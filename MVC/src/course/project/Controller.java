package course.project;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Controller {
    private View view;
    private OrderView orderView;
    private Model model;

    public Controller(View view, OrderView orderView, Model model)
    {
        this.view = view;
        this.model = model;
        this.orderView = orderView;
        this.view.updateTotalCost(new UpdateCalculation());
        this.view.finalCostView.totalSummaryActionListener(new ButtonActionListener());

    }

    class UpdateCalculation implements ChangeListener{

        public void stateChanged(ChangeEvent e) {
            try {
                model.calculate_total_cost(view.typeView.getSelection(), view.ingredientsView.get_checked_toppings_count());
                view.finalCostView.setTotalFeild(String.format("%.2f", model.getTotalCost()));
            }catch (Exception ex){
                view.finalCostView.setTotalFeild("Выберите пиццу");
            }
        }
    }

    class ButtonActionListener implements ActionListener{

        public void actionPerformed(ActionEvent e) {
            String finalOrder ="";
            try {
                finalOrder = "Имя: " + view.customerView._getName() +
                        "\nНомер телефона: " + view.customerView.getNumber() + "\nТип пиццы: " +
                        view.typeView.getSelection() + "\nДополнительные ингредиенты: " + view.ingredientsView.get_checked_toppings() + "\nИтого: " + String.format("%.2f", model.getTotalCost());
                orderView.set_textarea(finalOrder);
            }catch (Exception ex){
                finalOrder = "Пожалуйства, выберите пиццу!";
                orderView.set_textarea(finalOrder);
            }
            orderView.set_visibility(true);
        }

    }

}
