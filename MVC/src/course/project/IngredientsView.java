package course.project;

import java.util.ArrayList;

import javax.swing.*;

public class IngredientsView extends JPanel {

    private ArrayList <JCheckBox> ingredients = new ArrayList<>();

    public IngredientsView(){
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        add(new JLabel("Дополнительные ингредиенты", JLabel.CENTER));

        ingredients.add(new JCheckBox("Томаты"));
        ingredients.add(new JCheckBox ("Грибы"));
        ingredients.add(new JCheckBox ("Ветчина"));
        ingredients.add(new JCheckBox ("Сладкий перец"));
        ingredients.add(new JCheckBox("Тонкое тесто"));
        ingredients.add(new JCheckBox ("Толстое тесто"));

        for (JCheckBox checkbox: ingredients){
            add(checkbox);
        }
    }


    int get_checked_toppings_count(){
        int temp_count = 0;
        for (JCheckBox checkbox : ingredients){
            if (checkbox.isSelected()){
                temp_count++;
            }
        }
        return temp_count;
    }
    String get_checked_toppings(){
        String selected_toppings = "";
        for (JCheckBox checkbox : ingredients){
            if (checkbox.isSelected()){
                selected_toppings = selected_toppings + checkbox.getText() + " ";
            }
        }
        return selected_toppings;
    }

}
