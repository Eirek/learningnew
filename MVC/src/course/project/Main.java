package course.project;

public class Main {

    public static void main(String[] args) {
        Model pizza = new Model();
        View mainForm = new View();
        OrderView order = new OrderView();
        Controller mainController = new Controller(mainForm, order, pizza);

    }
}
