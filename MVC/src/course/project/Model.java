package course.project;

public class Model {

    private final int veggi = 180;
    private final int cheese = 150;
    private final int meat = 200;
    private final int hawaii = 310;
    private final int bbq = 400;

    private float totalCost;

    public void calculate_total_cost(String pizzaType, int addIngredients){
        totalCost = 0;
        if (pizzaType!="" && pizzaType!=null){
            if(pizzaType.equalsIgnoreCase("Вегитарианская")){
                totalCost += veggi;
            }else if (pizzaType.equalsIgnoreCase("Сырная")){
                totalCost += cheese;
            }else if (pizzaType.equalsIgnoreCase("Мясная")){
                totalCost += meat;
            }else if (pizzaType.equalsIgnoreCase("Гавайская")){
                totalCost += hawaii;
            }else if (pizzaType.equalsIgnoreCase("BBQ")){
                totalCost += bbq;
            }
        }
        totalCost +=addIngredients*30;
    }

    public float getTotalCost(){
        return totalCost;
    }
}
