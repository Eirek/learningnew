package course.project;

import javax.swing.*;

public class TypeView extends JPanel{

    private ButtonGroup buttonGroup;

    private JRadioButton veggiOption;
    private JRadioButton cheeseOption;
    private JRadioButton meatOption;
    private JRadioButton hawaiiOption;
    private JRadioButton bbqOption;

    public TypeView() {
        buttonGroup = new ButtonGroup();

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        add(new JLabel("Тип пиццы", JLabel.CENTER));

        veggiOption = new JRadioButton("Вегетарианская");
        veggiOption.setActionCommand("Вегетарианская");

        cheeseOption = new JRadioButton("Сырная");
        cheeseOption.setActionCommand("Сырная");

        meatOption = new JRadioButton("Мясная");
        meatOption.setActionCommand("Мясная");

        hawaiiOption = new JRadioButton("Гавайская");
        hawaiiOption.setActionCommand("Гавайская");

        bbqOption = new JRadioButton("BBQ");
        bbqOption.setActionCommand("BBQ");

        buttonGroup.add(veggiOption);
        buttonGroup.add(cheeseOption);
        buttonGroup.add(meatOption);
        buttonGroup.add(hawaiiOption);
        buttonGroup.add(bbqOption);

        add(veggiOption);
        add(cheeseOption);
        add(meatOption);
        add(hawaiiOption);
        add(bbqOption);
    }

    public ButtonGroup getRadioGroup(){
        return buttonGroup;
    }
    public String getSelection(){
        ButtonModel buttonModel = buttonGroup.getSelection();
        String actionCommand = buttonModel.getActionCommand();
        return actionCommand;
    }
}
