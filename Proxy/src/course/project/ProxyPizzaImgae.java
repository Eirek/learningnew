package course.project;

public class ProxyPizzaImgae implements Image{

    private PizzaImage pizzaImage;
    private String fileName;

    public ProxyPizzaImgae(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if(pizzaImage == null){
            pizzaImage = new PizzaImage(fileName);
        }
        pizzaImage.display();
    }
}
