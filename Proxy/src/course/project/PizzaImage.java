package course.project;

public class PizzaImage implements Image {

    private String pizza;

    public PizzaImage(String pizzaImage){
        this.pizza = pizzaImage;
        loadImage(pizzaImage);
    }

    @Override
    public void display() {
        System.out.println("Показать изображение пиццы " + pizza);
    }

    private void loadImage(String fileName){
        System.out.println("Загрузка изображения, пожалуйста подождите " + fileName);
    }
}
