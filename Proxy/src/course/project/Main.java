package course.project;

public class Main {

    public static void main(String[] args) {
        Image image = new ProxyPizzaImgae("\"Pizza_menu.jpg\"");

        image.display();
        System.out.println("");

        image.display();
        image.display();
    }
}
