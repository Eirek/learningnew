package course.project;

public class VegetarianIngredientsFabric implements IngredientsFactory {

    @Override
    public Cheese getCheese() {
        return new MozzarellaCheese();
    }

    @Override
    public Dough getDough() {
        return new ThinDough();
    }

    @Override
    public Ham getHam() {
        return new SoyHam();
    }

    @Override
    public Vegetables[] getVegetables() {
       Vegetables vegetables[] = { new Garlic(), new Tomatoes(), new RocketLeaves(), new Spinach(), new RedPaper()};
       return vegetables;
    }

}
