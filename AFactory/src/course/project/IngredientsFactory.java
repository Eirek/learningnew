package course.project;

public interface IngredientsFactory {

    Dough getDough();
    Ham getHam();
    Cheese getCheese();
    Vegetables[] getVegetables();

}
