package course.project;

public class ClassicIngredientFactory implements IngredientsFactory {

    @Override
    public Cheese getCheese() {
        return new ParmesanCheese();
    }

    @Override
    public Dough getDough() {
        return new ThickDough();
    }

    @Override
    public Ham getHam() {
        return new ParmaHam();
    }

    @Override
    public Vegetables[] getVegetables() {
       // Vegetables vegetables[] = { new Garlic(), new Tomatoes(), new RocketLeaves()};
        return  new Vegetables[]{ new Garlic(), new Tomatoes(), new RocketLeaves()};
    }
}
