package course.project;

public class Main {

    private static IngredientsFactory getFactory(String type) {
        String name;
        switch (type) {
            case "Фирменная":
                name = "Фирменную";
                System.out.println("---Заказ на " + name + " пиццу---");
                return new ClassicIngredientFactory();
            case "Вегетарианская":
                name = "Вегетарианскую";
                System.out.println("---Заказ на " + name + " пиццу---");
                return new VegetarianIngredientsFabric();
            default:
                throw new RuntimeException("Такой пиццы нет в меню: " + type);
        }
    }

    public static void main(String[] args) {

        IngredientsFactory factory = getFactory("Фирменная");
        Vegetables veggie[] = factory.getVegetables();
        Cheese cheese = factory.getCheese();
        Ham ham = factory.getHam();
        Dough dough = factory.getDough();

        dough.doughType();
        cheese.cheeseType();
        ham.hamType();
        for (int i = 0; i < veggie.length; i++){
            System.out.println(veggie[i]);
        }






    }
}
