package course.project;

public interface PaymentStrategy {

    void pay(int amount);
}
