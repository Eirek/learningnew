package course.project;

public class CashStrategy implements PaymentStrategy {

    private int cashAmount;


    public CashStrategy(int cashAmount){
        this.cashAmount=cashAmount;
    }

    @Override
    public void pay(int amount) {
        System.out.println(amount + " выбрана оплата наличными.\n" + "Сумма сдачи с " + cashAmount +" составила: " + (cashAmount-amount));
    }

}
