package course.project;

public class Pizza {

    private String pizzaName;
    private int price;

    public Pizza(String pizzaName, int price){
        this.pizzaName =pizzaName;
        this.price=price;
    }

    public String getPizzaName() {
        return pizzaName;
    }

    public int getPrice() {
        return price;
    }

}
