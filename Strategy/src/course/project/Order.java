package course.project;

import java.util.ArrayList;
import java.util.List;

public class Order {

    List<Pizza> pizzas;

    public Order(){
        this.pizzas =new ArrayList<Pizza>();
    }

    public void addItem(Pizza pizza){
        this.pizzas.add(pizza);
    }

    public void removeItem(Pizza pizza){
        this.pizzas.remove(pizza);
    }

    public int calculateTotal(){
        int sum = 0;
        for(Pizza pizza : pizzas){
            sum += pizza.getPrice();
        }
        return sum;
    }

    public void pay(PaymentStrategy paymentMethod){
        int amount = calculateTotal();
        paymentMethod.pay(amount);
    }
}
