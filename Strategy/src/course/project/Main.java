package course.project;

public class Main {

    public static void main(String[] args) {
        Order order = new Order();

        Pizza pizza1 = new Pizza("Фирменная",480);
        Pizza pizza2 = new Pizza("Вегетарианская",550);
        Pizza pizza3 = new Pizza("Гавайская",600);

        order.addItem(pizza1);
        order.addItem(pizza2);
        order.addItem(pizza3);

        order.pay(new CashStrategy(5000));

        System.out.println("--------------------------");

        order.pay(new CreditCardStrategy("Ivan Korobov", "1234123412341234", "000", "12/22"));
    }
}

